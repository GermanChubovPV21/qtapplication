#include "training.h"

Exercise::Exercise() : title(""), description(""){};
Exercise::Exercise(string title, string description) : title(title), description(description) {};

vector<Exercise> Training::trainingPerform(string fileName)
{
    vector<Exercise> exrsises;

    ifstream fin(fileName);
    while (!fin.eof())
    {
        string title;
        getline(fin, title);

        string description, str;
        getline(fin, str);
        while (str != "")
        {
            description += '\n' + str;
            getline(fin, str);
        }

        Exercise exr(title, description);
        exrsises.push_back(exr);
    }

    return exrsises;
}

void Training::printExrsises()
{
    for (vector<Exercise>::iterator it = exrsises.begin(); it != exrsises.end(); ++it)
        QMessageBox::about(this, it->title, it->description);
}

vector<Exercise> Training::getExrsises()
{
    return exrsises;
}
