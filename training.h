#ifndef TRAINING_H
#define TRAINING_H

#include <iostream>
#include <string>
#include <vector>
#inlcude <iterator>
using namespace std;

class Exercise
{
public:
    string title;
    string description;
public:
    Exercise();
    Exercise(string title, string description);
};

class Training
{
private:
    vector<Exercise> exrsises;
    int current;
public:
    Training() : exrsises(NULL), current(0) {};
    Training(string fileName) : current(0) { exrsises = trainingPerform(fileName); }
    vector<Exercise> getExrsises();
    void printExrsises();
private:
    vector<Exercise> trainingPerform(string fileName);
};

#endif // TRAINING_H