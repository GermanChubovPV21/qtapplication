#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "exercisewindow.h"
#include "ui_exercisewindow.h"
#include <QApplication>
#include <QMessageBox>
#include <QtCore/QCoreApplication>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_1_clicked()
{
    Training everyday("everyday.txt");
    everyday.printExrsises();
}

void MainWindow::on_pushButton_2_clicked()
{
    Training intense("intense.txt");
    intense.printExrsises();
}

void MainWindow::on_pushButton_3_clicked()
{
    Training gym("gym.txt");
    gym.printExrsises();;
}
