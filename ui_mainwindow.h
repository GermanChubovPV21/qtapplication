/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Mon 18. May 09:15:23 2020
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QPushButton *pushButton_1;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(442, 311);
        MainWindow->setStyleSheet(QString::fromUtf8("background-color: #363636;\n"
"Color: #fff !important;\n"
"font-size: 12 !important;\n"
"font-style: PT Sans !important;"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout->setContentsMargins(15, -1, 15, 0);
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setStyleSheet(QString::fromUtf8("font-family: PT Sans;\n"
"font-size: 14px;\n"
"color:#fff;\n"
"text-align:center;\n"
"font-weight: bold;\n"
"padding-bottom: -15px;\n"
""));

        verticalLayout->addWidget(label);

        pushButton_1 = new QPushButton(centralWidget);
        pushButton_1->setObjectName(QString::fromUtf8("pushButton_1"));
        sizePolicy.setHeightForWidth(pushButton_1->sizePolicy().hasHeightForWidth());
        pushButton_1->setSizePolicy(sizePolicy);
        pushButton_1->setMouseTracking(false);
        pushButton_1->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"font-family: PT Sans;\n"
"font-size: 14px;\n"
"color:#fff;\n"
"text-align:center;\n"
"border-radius: 5px;\n"
"background-color: #ff2b27;\n"
"font-weight: bold;\n"
"text-transform: uppercase;\n"
"padding: 18px 31px;\n"
"margin-left:7px;\n"
"margin-bottom: 3px;\n"
"border-bottom: 4px solid #C2120F;\n"
"letter-spacing:0.5px;\n"
"}\n"
"QPushButton:hover {\n"
"background-color:#e2231f;\n"
"}"));

        verticalLayout->addWidget(pushButton_1);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        sizePolicy.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
        pushButton_2->setSizePolicy(sizePolicy);
        pushButton_2->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"font-family: PT Sans;\n"
"font-size: 14px;\n"
"color:#fff;\n"
"text-align:center;\n"
"border-radius: 5px;\n"
"background-color: #ff2b27;\n"
"font-weight: bold;\n"
"text-transform: uppercase;\n"
"padding: 18px 31px;\n"
"margin-left:7px;\n"
"margin-bottom: 3px;\n"
"border-bottom: 4px solid #C2120F;\n"
"letter-spacing:0.5px;\n"
"}\n"
"QPushButton:hover {\n"
"background-color:#e2231f;\n"
"}"));

        verticalLayout->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        sizePolicy.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
        pushButton_3->setSizePolicy(sizePolicy);
        pushButton_3->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"font-family: PT Sans;\n"
"font-size: 14px;\n"
"color:#fff;\n"
"text-align:center;\n"
"border-radius: 5px;\n"
"background-color: #ff2b27;\n"
"font-weight: bold;\n"
"text-transform: uppercase;\n"
"padding: 18px 31px;\n"
"margin-left:7px;\n"
"margin-bottom: 3px;\n"
"border-bottom: 4px solid #C2120F;\n"
"letter-spacing:0.5px;\n"
"}\n"
"QPushButton:hover {\n"
"background-color:#e2231f;\n"
"}"));

        verticalLayout->addWidget(pushButton_3);


        verticalLayout_2->addLayout(verticalLayout);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:x-large; font-weight:600;\">Choose your training:</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        pushButton_1->setText(QApplication::translate("MainWindow", "Everyday training at home", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("MainWindow", "Intensive training at home", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("MainWindow", "Training at gym", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
